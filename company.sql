-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2018 at 04:46 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `company`
--

-- --------------------------------------------------------

--
-- Table structure for table `companydata`
--

CREATE TABLE `companydata` (
  `NO` int(5) NOT NULL,
  `CompanyName` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `typeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companydata`
--

INSERT INTO `companydata` (`NO`, `CompanyName`, `Address`, `PhoneNumber`, `Email`, `typeID`) VALUES
(1, 'New Starlight', '73th Thazin Road,Mandalay', '02404562', 'www.newstarlight.com', 1),
(2, 'Myanmar Delight Travel', '88th street,Yaw Min Gyi Street,Mandalay', '0969302020', 'www.mmdelight.com', 2),
(3, 'Mighty Beat Myanmar', '88th Street,Yaw Min Gyi Street,Mandalay', '0969302020', 'www.mighty.com', 2),
(4, 'UAB Bank', '88th Street,Yaw Min Gyi Street,Mandalay', '0969302020', 'www.uab.com', 3),
(5, 'Binaru to Reality', '33th Street,Bet 83th and 84th Street,Mandalay', '092020840', 'www.binaru.com', 3),
(6, 'Than Brother', 'Yaw Min Gyi Street,Mandalay', '097847628', 'www.thanbrother.com', 3),
(7, 'NEC Company', 'Corner of 34th and 84th Street,Mandalay', '092150200', 'www.nec.com', 3),
(8, 'Seven Copy Company', 'Corner of 73th and 37th Street,Mandalay', '092019846', 'www.sevencopy.com', 4),
(9, 'Mobile Mother', '26th Street,Bet 76th and 77th Street,Mandalay', '097920419', 'www.mmother.com', 4),
(10, 'Central Computer', 'Corner of 40th and 73th Street,Mandalay', '09444008462', 'www.central.com', 4),
(11, 'Eastern World', '82nd and 28th Street,Mandalay', '097327744', 'www.eastern.com', 3),
(12, 'Forward', '82nd and 28th Street,Mandalay', '0274780', 'www.forward.com', 3),
(13, 'Yangon Airways', 'Room3,78th Street,Mandalay', '0934405', 'www.ygnairway.com', 2),
(14, 'ACD Gallery', 'Kyansitthar Street,Mandalay', '0267025', 'www.acd.com', 3),
(15, 'ACC Computer Center', '84th Street,Bet 31st and32nd Street,Mandalay', '0265566', 'www.acc.com.mm', 4),
(16, 'Carat', '17th Street,Bet 32nd and 33th Street,Mandalay', '092003430', 'www.carat.com', 3),
(17, 'Kyaw Swar Win(LCCI)', '37th and 38th Street,Mandalay', '0262921', 'www.kyawswarwin.com', 4),
(18, 'Mann Thiri', '81th and 82th Street,Mandalay', '0265828', 'www.mannthiri.com', 2),
(19, 'Twilight Travel Company', '31st and 32nd Street,Mandalay', '0234603', 'www.twilight.com', 2),
(20, 'Laytan Marksman', '73th Street,Thazin Street,Mandalay', '02404562', 'www.marksman.com', 3),
(21, 'Roger Fighter', '88th Street,Yaw Min Gyi Street,Mandalay', '0969302020', 'www.fighter.com', 3),
(22, 'Kagura Mage', '25th street and 26th Street,Mandalay', '0969400400', 'www.mage.com', 3),
(23, 'Fanny Assion', '88th,Yaw Min Gyi Street,Mandalay', '09794367342', 'www.fanny.com', 3),
(24, 'DrDr', '32th,Bet 82th and83th Street,Mandalay', '09797294073', 'www.drdr.com', 3),
(25, 'Kwee Kyaw Thura', 'Kathar Lann Street,Mandalay', '0943029989', 'www.kweekyaw.com', 3),
(26, 'KT Mdy Construction', '35th Street,Bet 68th and 69th Street,Mandalay', '022832162', 'www.ktconstructionmdy.com', 1),
(27, 'Best Oil Company(BOC)', 'Theik Pan Street,Mandalay', '094024562', 'www.boc.com', 5),
(28, 'Information Technology Security', '78th,Yaw Min Gyi Street,Mandalay', '09423372706', 'www.itsg.mdy.com', 4),
(29, 'Innova Mandalay', '41th Street,Bet 87th street and 86th Street Mandalay', '09975671639', 'www.innova.com', 6),
(30, 'North Wings', 'Corner of 62nd and Zalatwar Street,Mandalay', '09973684354', 'www.northwings@gmail.com', 7),
(31, 'Talents in Mandalay', 'Diamond Plazza,4th Floor,Mandalay', '092023575', 'talentsmdy@gmail.com', 3),
(32, 'MDY Point', '42nd Theikpan Street,Mandalay', '09265388885', 'mdypointtravel@gmail.com', 2),
(33, 'MDY Certified Net', '29th Street,Bet 62th and 63th Street,Mandalay', '09774825639', 'mdycertifiednet@gmail.com', 4),
(34, 'MDY Management Coorperation', 'Mingalar Mandalay Street,Mandalay', '092023575', 'mdyMC@gmail.com', 6),
(35, 'Unlink Myanmar', '66th Street,Corner of 39th Street,MaharAungMyay,Mandalay', '097663399998', 'www.unilink.com.mm', 4),
(36, 'Orange Mobile', '31st Street,corner of 81th Street,Mandalay', '09775743567', 'www.orange.com', 4),
(37, 'Natural Dream', '88th Street,Bet 18th and 19th Street,Mandalay', '0944401202', 'www.naturaldream.com', 1),
(38, 'MyCom', 'corner of 32nd and 77th street,Mandalay', '0269309', 'www.mycom.com', 4),
(39, 'Net Plus Myanmar', '68th Street,Mandalay', '097633253568', 'www.netplusmm.com', 4),
(40, 'Orient Software Devlopment', '28th Street,Bet 70th and 71th Street,Mandalay', '09441037890', 'www.orient.com.mm', 4),
(41, 'Heritage MDY', '78th Street,Bet 69th and 70th Street,Mandalay', '09421911919', 'www.heritagemdy.com', 2),
(42, 'Happy CCTV Service', '41 st,90th and 91th Street,Mandalay', '09976774320', 'www.happycctv.com', 4),
(43, 'Mega Mobile Service', '29st,Bet 81th Street and 82th Street Mandalay', '0271083', 'www.mega.com', 2),
(44, 'Unique Super Power', '68th Street,Bet 33th and 34th Street,Mandalay', '095107663', 'www.usp.com.mm', 1),
(45, 'Moon Eye', '33th Street,Bet 76th Street and 77th Street Mandalay', '09441040045', 'www,mooneye.com', 3),
(46, 'TVS Motor Cycle', '63th and 64th Street,Theik Pan Road,Mandalay', '0271083', 'www.tvs.com', 7),
(47, 'X-Sport', '78th Street,Bet 32th Street and 33th Street,Mandalay', '0949214587', 'www.xsport.com', 3),
(48, 'Info Net Electronic', 'Mingalar Mandalay Block2,mandalay', '09781325966', 'www.infonetmm.com', 3),
(49, 'Bagan Wavw', 'Bet 57th and 58th Street,Theikpan Street,Mandalay', '095107261', 'www.baganwave.com', 4),
(50, 'Point', 'Bet 31th and 32th Street,Mandalay', '0971654', 'www.point.com', 3),
(51, 'Royal oasis', '27th Street,Bet 72th and 73th Street,Mandalay', '0265113', 'royaloasismm@gmail.com', 9),
(52, 'Min Naing Khant', '42nd Street,bet 62nd and 63rd Street,mandalay', '09420039333', 'minnaingkhantmm@gmail.com', 1),
(53, 'Great Diamond', '80th Street,bet 32th and 33rd Street,Mandalay', '0235235', 'greatdiamondmm@gmail.com', 3),
(54, 'Polygold', '36th Street,Bet 60th and 61th Street,Mandalay', '0239926', 'polygoldmm@gmail.com', 3),
(55, 'All The Best', 'Min Gyi Yan Naung Street and 52nd Street Corner,Mandalay', '09789107355', 'allthebestmm@gmail.com', 4),
(56, 'Win Sabei', '27th Street,bet 82nd and 83th Street,Mandalay', '09420115887', 'winsabeimm@gmail.com', 8),
(57, 'May and Mark Gems', '30th Street,bet 73th and 74th Street,Mandalay', '0233229', 'myanmarkmm@gmail.com', 3),
(58, 'LM Travel Myanmar', 'Flat no25,room31,Myayinandar housing Mandalay', '0933048696', 'lmtravelmm@gmail.com', 2),
(59, 'Binary to Reality', '33rd street,bet 83th and 84th Street,mandalay', '092020841', 'binarytorealitymm@gmail.com', 4),
(60, 'Peninsula Enterprises', '26th(B)street,bet 83rd and 84th Street,Mandalay', '0236720', 'peninsulaasiamm@gmail.com', 4),
(61, 'Orient Software Development', 'Bet 84th and 85th Street,Mandalay', '0991037890', 'orientsoftwaremm@gmail.com', 4),
(62, 'Htilar(Than Mg & Brothers)', 'Top of Yaw Min Gyi Street,Industrial Zone1,Mandalay', '095133611', 'htilarburma@gmail.com', 1),
(63, 'Leo', '42nd Street,Bet 62th and 63rd street,Mandalay', '092038747', 'leoburma@gmail.com', 1),
(64, 'Dana Thukha', 'No 102/39th street,Mandalay24,Bet 38 rd and ', '095115784', 'danathukhamm@gmail.com', 3),
(65, 'Kabar Kyaw Trading', 'Mingalar Market,73rd Street,Bet 30th and 31th street Mandalay', '0261626', 'kabarkyawburma@gmail.com', 8),
(66, 'Mandalay Certified Net', '29th Street,Bet 62th and 63th Street,Mandalay', '09774825639', 'mandalaycertifiednet@gmail.com', 4),
(67, 'Golden City', 'No.436/31,Bet 33rd and 34th Street,Mandalay', '0234886', 'goldencitymm@gmail.com', 3),
(68, 'The Analystic Company', '30th street,Bet 65th and 66th street,Mandalay', '0274743', 'theanalysismm@gmail.com', 4),
(69, 'Diamond Palace', 'Corner of 73rd and 28th Street,Mandalay', '092017009', 'diamondpalace@gmail.com', 3),
(70, 'Icon Mandalay', '86th Street,Bet 44th and 45th Street,Mandalay', '0995263276', 'iconmandalay@gmail.com', 1),
(71, 'Three Swallows', '77th Street,Bet 31st and 32nd Street,Mandalay', '0274461', 'threeswallowmm@gmail.com', 1),
(72, 'Skylark', '26th Street,bet 75th and 76th street,Mandalay', '0234719', 'skylarkmyanmar', 3),
(73, 'Super Seven Star', 'No34,Ygn-Mdy Highway road,mandalay', '0278919', 'mdy@superseven.com', 3),
(74, 'Mega Lifescience', 'No15/5,55th Street and 43th Street,mandalay', '092832683', 'info@megawecare.com', 3),
(75, 'Super Seven Star Motor Industry', 'Corner of Ygn-Mdy and Kyawe Sae Kan 11 road,Mandalay\r\n', '09977442259', 'mdy@superseveven.com', 3),
(76, 'Avamin Co.ltd', '81th Street,Bet 32th and 33th Street,Mandalay', '0239725', 'avaminspirulina@gmail.com', 3),
(77, 'Chin Thaung Tan Co,ltd', 'No2,Thiri Mandalar car compound,mandalay', '092450355', 'www.chinthaungthan@gmail.com', 3),
(78, 'One &Only Fine Drinking', '26th Street,Bet 63th and 64th Street,Mnadalay', '09257170007', 'www.oneandonly@gmail.com', 9),
(79, 'Pyin Oo Lwin Real Estate', 'No135,Hanee pagoda road,mandalay', '0943164723', 'www.polrealestate@gmail.com', 3),
(80, 'One Nine Street Treding', '28th street,Bet 80th and 81th Street,Mandalay', '0222604', 'www.oneninestreet@gmail.com', 9),
(81, 'GEO Teach Pile', 'No255,35th street,bet 80th and 81th Street Mandalay', '0267703', 'www.geoteachpile@gmail.com', 4),
(82, 'CAD Construction', 'No122,Cor 80th and 31th Street,Mandalay', '0264571', 'www.codeconstruction@gmail.com', 1),
(83, 'ADK Advertising ', '65th Street,Bet 26th and 27th Street Mandalay', '092039016', 'www.cdkdvertising@gmail.com', 3),
(84, 'Royal Myanmar Advertising', '86th Street,Bet 42th and 43th Street,Mandalay', '0271515', 'www.royal@gmail.com', 3),
(85, 'Mann Shin Oxygen', 'Ygn-Mdy Road,Industrial Zone,Mandalay', '095188448', 'www.mannshannoxygen@gmail.com', 5),
(86, 'Active Multi Media', '31st and 32nd Street/77th and 78th Street,Mandalay', '0269320', 'www.activemutimedia@gmail.com', 3),
(87, 'Future Technlogy', '82th Street,bet 30th and 31th Street,Mandalay', '0244363', 'www.futuretechnology@gmail.com', 4),
(88, 'TW General Trading', 'Kha Yay Street,Industrial Zone1,Mandalay', '095153298', 'www.omiemyanmar@gmal.com', 3),
(89, 'MDY Cement Company', 'Patheingyi,mandalay', '0244537', 'www.myanmarelephant@gmail.com', 3),
(90, 'Hein Engineering', '61st,Industrial Zone1,Mandalay', '0234824', 'www.heinengineering@gmail.com', 1),
(91, 'Grace Trading', 'No f5, industrial Zone1,Mandalay', '02543094', 'www.gracetrading@gmail.com', 3),
(92, 'Silver Tiger', '26th street,bet 83th and 84th Street,Mandalay', '0234957', 'www.silvertiger@gmail.com', 3),
(93, 'Nay Zun One Company', '36th Street,Bet 83th and 84th Street,Mandalay', '0266216', 'www.rayzunone@gmail.com', 3),
(94, 'Ayar Shwe Wah Company', '6th Street,Kyawe Sae Kan,mandalay', '0259202', 'www.ayarshwewah@gmail.com', 3),
(95, 'Mangiri Indutrial Company', 'No 267,Ponnarsu Street,mandalay', '0221935', 'www.mangiri@gmail.com', 1),
(96, 'Bayin Biscuit', 'Pyi Gyi Tagon ,Mandalay', '0280540', 'www.bayin@gmail.com', 9),
(97, 'Shwe Htate Tan', 'No 45,Bet 85th and 86th Street,mandalay', '0224039', 'www.shwehtatetan@gmail.com', 3),
(98, 'Five Star Company', '83th Street,Bet 32th and 33th Street,Mandalay', '0224889', 'www.fivestar@gmail.com', 3),
(99, 'Shoe gallery Company', '4th floor,78th Shopping mall,Mandalay', '0271470', 'www.shoegallery@gmail.com', 3),
(100, 'Aung Min Ko &Brothers Company', 'Zay Cho Ground Floor,mandalay', '0236452', 'www.aungminko@gmail.com', 3);

-- --------------------------------------------------------

--
-- Table structure for table `companytype`
--

CREATE TABLE `companytype` (
  `typeID` int(10) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `companytype`
--

INSERT INTO `companytype` (`typeID`, `type`) VALUES
(1, 'Construction'),
(2, 'Travel and Tour'),
(3, 'Business'),
(4, 'Technology'),
(5, 'Oil and Gas'),
(6, 'Management'),
(7, 'Motorcycle'),
(8, 'Trading'),
(9, 'Food and Drink');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `companydata`
--
ALTER TABLE `companydata`
  ADD PRIMARY KEY (`NO`);

--
-- Indexes for table `companytype`
--
ALTER TABLE `companytype`
  ADD PRIMARY KEY (`typeID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `companydata`
--
ALTER TABLE `companydata`
  MODIFY `NO` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `companytype`
--
ALTER TABLE `companytype`
  MODIFY `typeID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
