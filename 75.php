<!DOCTYPE html>
<html>
<head>
<style>
body{background-color:white;}
div{
	background-color:lightblue;
	color:black;
	padding:10px;
	width:100%;
	line-height:1.5em;
}
footer{
	background-color:gray;
	padding:10px; 
	text-align:center;
}
</style>
</head>
<body>
<h2 style="color:indigo; text-align:center;">SUPER SEVEN STAR MOTOR INDUSTRY CO.,LTD</h2>
<div> 
<h3 style="color:purple;">-COMPANY NAME</h3>
<p style="color:red-purple;">Super Seven Star Motor Industry Co.,Ltd</p>
<h3 style="color:purple;">-ADDRESS</h3>
<p style="color:red-purple;">
No.704,Corner of Yangon-Mandalay Road & Kywe Sal Kan(11) Road,<br>
Chan Mya Tharyar Quarter,
Pyi Gyi Tagon Township,Mandalay,<br>
Mandalay,Mandalay,Myanmar</p>
<h3 style="color:purple">-PHONE</h3>
<p style="color:red-purple;">
09-977442259
</p>
<h3 style="color:purple;">-E-MAIL</h3>
<p style="color:red-purple;">mdy@supersevenstarsmotorindustry.com</p>
</div><hr>
<div>
<h3 style="color:purple;">-DESCRIPTION</h3>
Super Seven Star Internationl Trading Co.,ltd was a trading firm selling Japanese
electronic appliances as well as passenger and vehicles and automobile spare parts.
Currently,Super Seven Star Co.,Ltd sells Kia passenger cars (Compacts
,Saloons,SUV,MPV,Light Trucks)are imported and sold throughout Myanmar throughout
8 Sales Showrooms.After receiving SsangYong Motors Co.,Ltd's Exclusive Distributorship 
agreement,SSS has been selling SUV,MPV and Double Cabin Pickup cars through our sales
outlets across Myanmar.
</div><hr>
<div>
<h3 style="color:purple;">-CORE VALUES</h3>
<h4 style="color:green;">-Passion and Enthusiasm</h4>
Passionately striving for the best results and enjoying all the fun in the process

<h4 style="color:green;">-Being Different</h4>
Achiving objectives in a creative,innovative way by thinking out of the box
<h4 style="color:green;">-Pursuit of Excellence</h4>
Taking up new initiatives,challenges,and doing the job 
right the first time
<h4 style="color:green;">-Team Sprit</h4>
Working as a cohesive and disciplined team towards our common
goals and concerted efforts
<div><hr>
<h3 style="color:purple;">WORKING HOURS</h3>
<p style="color:green;">
Monday:      9am to 5:00pm<br>
Tuesday:     9am to 5:00pm<br>
Wednesday:   9am to 5:00pm<br>
Thursday:    9am to 5:00pm<br>
Friday:      9am to 5:00pm<br>
Saturday:    9am to 5:00pm<br>
Sunday:      9am to 5:00pm<br>
</p>
</div>


<footer>
<p style="font-size:20px; color:yellow;">Super Seven Stars Motor Industry    Co.,Ltd</p>
<p style="color:white;">Registered in Myanmar.All rights reserved.</p>
</body>
</html>
